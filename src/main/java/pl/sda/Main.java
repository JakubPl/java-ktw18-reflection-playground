package pl.sda;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

public class Main {
    public static void main(String[] args) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException, NoSuchFieldException {
        String name = "Jan";
        String surname = "Kowalski";
        /*<man>
                <name>Jan</name>
                <surname>Kowalski</surname>
               </man>
      */

        Constructor<MyTestClass> constructor = MyTestClass.class.getConstructor();
        MyTestClass myTestClassObject = constructor.newInstance();

        Field fieldName = MyTestClass.class.getDeclaredField("name");
        Field fieldSurname = MyTestClass.class.getDeclaredField("surname");

        fieldName.setAccessible(true);
        fieldSurname.setAccessible(true);

        fieldName.set(myTestClassObject, name);
        fieldSurname.set(myTestClassObject, surname);

        System.out.println(myTestClassObject);


    }
}

package pl.sda;

//TODO: jak sprawdzac pokrycie kodu testami
public class MyTestClass {
    private String name;
    private String surname;

    public MyTestClass() {
    }

    public MyTestClass(String name, String surname) {
        this.name = name;
        this.surname = surname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    @Override
    public String toString() {
        return "MyTestClass{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                '}';
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }
}
